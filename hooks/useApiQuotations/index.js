import { useEffect, useState } from "react";
import io from "socket.io-client";

const apiSocketConfig = {
  query: {
    token: process.env.NEXT_PUBLIC_TRADE_TOKEN,
    portfolioId: process.env.NEXT_PUBLIC_TRADE_PORTFOLIO_ID,
  },
  transports: ["websocket"],
};

const apiSocketUrl = `${process.env.NEXT_PUBLIC_KINVO_API_URL}/trade`;

export function useApiQuotations(selectedTickers = []) {
  const [quotations, setQuotations] = useState([]);
  const [socket, setSocket] = useState(null);
  const [previousFavQuotations, setPreviousFavQuotations] = useState([]);

  useEffect(() => {
    const socket = io(apiSocketUrl, apiSocketConfig);
    setSocket(socket);

    socket.on("connect", () => {
      console.log("[socket] connected at: ", new Date());
    });

    socket.on("disconnect", () => {
      console.log("[socket] disconnect at: ", new Date());
    });
  }, []);

  useEffect(() => {
    if (!socket) return;

    function handler(data) {
      const updatedQuotations = data.favoriteTickers.filter((quotation) => {
        const previousQuotation = previousFavQuotations.find(
          (_quotation) => quotation.ticker === _quotation.ticker
        );
        return (
          !previousQuotation ||
          previousQuotation.lastUpdate !== quotation.lastUpdate
        );
      });
      console.log("[socket] new quotation: ", data);

      if (!updatedQuotations.length) {
        console.log(
          "[socket] ticker não atualizado neste evento",
          updatedQuotations,
          data.favoriteTickers
        );
        return;
      }

      setPreviousFavQuotations(data.favoriteTickers);

      const seletecUpdatedQuotations = updatedQuotations.filter((quotation) =>
        selectedTickers.includes(quotation)
      );

      setQuotations((value) => [
        ...value,
        ...seletecUpdatedQuotations.map((quotation) => ({
          ...quotation,
          at_ms: new Date().getTime(),
        })),
      ]);
    }

    socket.on("live-follow-up:update", handler);
    return () => socket.off("live-follow-up:update", handler);
  }, [socket, selectedTickers]);

  return [quotations, socket];
}
