const Redis = require("ioredis");

const channelPrefix = process.env.REDIS_CHANNEL;

import { Server } from "socket.io";

const ioHandler = (req, res) => {
  if (!res.socket.server.io) {
    console.log("*First use, starting socket.io");

    const io = new Server(res.socket.server, {
      transports: ["websocket"],
    });

    io.on("connection", (socket) => {
      socket.broadcast.emit("a user connected");

      const redis = new Redis({
        port: process.env.REDIS_PORT, // Redis port
        host: process.env.REDIS_HOST, // Redis host
        tls: true,
        password: process.env.REDIS_PASS,
        db: 0,
      });

      redis.on("message", (channel, message) => {
        const product = JSON.parse(message);
        socket.emit("new-quotation", product);
      });

      socket.on("live-follow-up:add-favorite-tickers", (data) => {
        const { tickers } = data;
        const channels = tickers.map(
          (ticker) => `${channelPrefix}:${ticker.toLowerCase()}`
        );
        redis.subscribe(channels, (err, count) => {
          if (err) console.error(err.message);
          console.log(`Subscribed to ${count} channels.`);
        });
      });
    });

    res.socket.server.io = io;
  } else {
    console.log("socket.io already running");
  }
  res.end();
};

export const config = {
  api: {
    bodyParser: false,
  },
};

export default ioHandler;
