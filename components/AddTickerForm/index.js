import React, { useEffect, useState } from "react";
import { useDebounce } from "../../hooks";

export function AddTickerForm(props) {
  const { onSubmit, onAddTicker } = props;

  const [suggestions, setSuggestions] = useState([]);
  const [ticker, setTicker] = useState("");

  const debouncedTicker = useDebounce(ticker, 300);

  useEffect(() => {
    if (!debouncedTicker) setSuggestions([]);
    fetch(
      `${process.env.NEXT_PUBLIC_KINVO_API_URL}/portfolio-command/bovespaStock/GetByFilter/${ticker}`,
      {
        headers: {
          authorization: `Bearer ${process.env.NEXT_PUBLIC_TRADE_TOKEN}`,
        },
      }
    )
      .then((res) => res.json())
      .then((res) => {
        if (res.success) {
          setSuggestions(res.data);
        }
      });
  }, [debouncedTicker]);

  async function submit(event) {
    event.preventDefault();
    if (!ticker) {
      return;
    }
    await onSubmit(ticker);
    onAddTicker(ticker);
    setTicker("");
    setSuggestions([]);
  }

  function renderSuggestions() {
    return (
      <ul>
        {suggestions.map((suggestion) => (
          <li key={suggestion.id}>{suggestion.code}</li>
        ))}
      </ul>
    );
  }

  return (
    <div>
      <h2>Adicionar novo Ticker</h2>
      <form onSubmit={submit}>
        <div className="d-flex">
          <input
            name="ticker"
            value={ticker}
            onChange={(event) => setTicker(event.target.value.toUpperCase())}
            placeholder="Ticker"
          />
          <input type="submit" value="Adicionar" />
        </div>
        {renderSuggestions()}
      </form>
    </div>
  );
}
